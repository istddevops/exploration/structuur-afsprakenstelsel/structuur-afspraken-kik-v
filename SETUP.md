# Opzet GoHugo Book Project Template

Dit GoHugo Project Template is opgezet op basis van Go Modules om te zorgen voor flexibileit in gebruik en uitbreidbaarheid van beschikbare **Themes** en eigen maatwerk daarop. Zie voor achtergrond https://gohugo.io/hugo-modules/use-modules/

## Installatie lokale afhankelijkheden

Om de opzet met Go Modules lokaal te kunnen doorvoeren zijn de volgende CLI-tools geinstalleerd:

- [X] MacOS
  - brew install hugo
  - brew install go
- [ ] Windows

## Repository configureren

In de root van de repository moet een **`go.mod`** configuratie-bestand met onderstaande inhoud (met verwijzing naar de GitLab repository) worden geplaatst.

```go
module gitlab.com/istddevops/shared/gohugo/gohugo-book-project-template

go 1.17
```

Daarna is het `GoHugo Book Theme` en `GoHugo Book Template` (met specifieke aanvullingen) aan het `config/_default/config.yml` bestand toegevoegd.

```yaml
theme: 
  - gitlab.com/istddevops/shared/gohugo/gohugo-book-template
  - github.com/alex-shpak/hugo-book
```
