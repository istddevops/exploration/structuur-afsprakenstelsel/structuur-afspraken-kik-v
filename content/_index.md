---
title: Introductie
description: >
  Evaluatie van de huidige [KIK-V Afsprakenset publicatie](https://kik-v.gitlab.io/afsprakenset) tegen uitgangspunten voor de oplevering van documentatie vanuit het iWlz actieprogramma (bron: presentatie van *Jeroen & Jorrit*).
bookToC: true
---

| Versie | Status | Auteur(s) | Omschrijving |
|:-|:-|:-|:-|
| 0.0.1 | Intitieel | Onno Haldar | Quick Scan (wat nu opvalt en/of ter discussie staat) |

## Structuur Afsprakenstelsel

```plantuml
@startuml introductie
skinparam handwritten true
!$pathDocs = "<%= docsURL %>"
!$pathInleiding = $pathDocs + "/inleiding"
!$pathAfsprakenset = $pathDocs + "/afsprakenset"
!$pathBestuurlijk = $pathAfsprakenset  + "/bestuurlijk"
!$pathGeneriek = $pathAfsprakenset  + "/generiek"
!$pathProces = $pathAfsprakenset  + "/proces"

package "PUBLICATIE" #Olive {

  package "Inleiding" as Inleiding #LightGrey {
  
    rectangle "Business Case" as BC %darken("LightGrey", 25) {

    }

    rectangle "Opdrachtformulering" as Opdrachtformulering %darken("LightGrey", 25) {

    }

    rectangle "Toelichting" as Toelichting %darken("LightGrey", 25) {

    }

    rectangle "Achtergrond" as Achtergrond %darken("LightGrey", 25) {

    }

    url of Toelichting is [[$pathInleiding/toelichting{Evaluatie Toelichting}]]
    url of Opdrachtformulering is [[$pathInleiding/opdrachtformulering{Evaluatie Opdrachtformulering}]]
    url of BC is [[$pathInleiding/business-case{Evaluatie Business Case}]]
    url of Achtergrond is [[$pathInleiding/achtergrond{Evaluatie Achtergrond}]]
  }

  package "Afsprakenset" as Afsprakenset #FloralWhite {

    package "Bestuurlijk" as Bestuurlijk %lighten("Gold", 50) {

      rectangle "Releasebeleid" as Releasebeleid %darken("Gold", 10) {

      }

      rectangle "SLA's" as SLAs %darken("Gold", 10) {

      }

      rectangle "Convenant KIK-V" as Convenant %darken("Gold", 10) {

      }

      url of Releasebeleid is [[$pathBestuurlijk/releasebeleid{Evaluatie Releasebeleid}]]
      url of SLAs is [[$pathBestuurlijk/slas{Evaluatie SLAs}]]
      url of Convenant is [[$pathBestuurlijk/convenant{Evaluatie Convenant}]]
    }

    package "Generiek" #Lightgreen {

      rectangle " Concretisering\nArchitectuur &\nPve" as Architectuur_Pve #LimeGreen {

      }

      rectangle "Model-\nuitwisselprofiel" as Modeluitwisselprofiel #LimeGreen {

      }

      rectangle "Conceptueel\nModel \n(gegevensset)" as Conceptueel_Model #LimeGreen {

      }

      rectangle "Uitgangspunten & \nRandvoorwaarden" as Uitgangspunten_Randvoorwaarden #LimeGreen {

      }

      url of Uitgangspunten_Randvoorwaarden is [[$pathGeneriek/uitgangspunten-randvoorwaarden{Evaluatie Uitgangspunten & Randvoorwaarden}]]
      url of Conceptueel_Model is [[$pathGeneriek/conceptueel-model{Evaluatie Conceptueel_Model}]]
      url of Modeluitwisselprofiel is [[$pathGeneriek/modeluitwisselprofiel{Evaluatie Modeluitwisselprofiel}]]
      url of Architectuur_Pve is [[$pathGeneriek/architectuur-pve{Evaluatie Architectuur & Pve}]]
    }

    package "Proces" %lighten("Violet", 10) {

      rectangle "Scenario's\n(Toepassing &\nUitwisselkalender)" as Scenarios %darken("Violet", 10) {

      }

      rectangle "Procesmodel\n(Beheer &\n Ontwikkeling)" as Procesmodel %darken("Violet", 10) {

      }

      url of Procesmodel is [[$pathProces/procesmodel{Evaluatie Procesmodel}]]
      url of Scenarios is [[$pathProces/scenarios{Evaluatie Scenario's}]]
    }

    package "Specificatie" #LightSkyBlue {

      rectangle "Uitwisselprofiel\n(n)" #DeepSkyBlue {

      }

      rectangle "Uitwisselprofiel\n(n - 1)" #DeepSkyBlue {

      }

      rectangle "Uitwisselprofiel\n(...)" #DeepSkyBlue {

      }

      rectangle "Uitwisselprofiel\nPersoneel & Veiligheid" #DeepSkyBlue {

      }

    }

    url of Bestuurlijk is [[$pathBestuurlijk{Evaluatie Bestuurlijk}]]
    url of Generiek is [[$pathGeneriek{Evaluatie Generiek}]]
    url of Proces is [[$pathProces{Evaluatie Proces}]]
    "Bestuurlijk" -[hidden]-> "Generiek"
    "Generiek" -[hidden]-> "Proces"
    "Proces" -[hidden]-> "Specificatie"
  }

  url of Inleiding is [[$pathInleiding{Evaluatie Inleiding}]]
  url of Afsprakenset is [[$pathAfsprakenset{Evaluatie Afsprakenset}]]
  "Inleiding" -[hidden]-> "Afsprakenset"
}

@enduml
```

## Samenvatting evaluatie

{{< nextinsectiontable >}}
