---
title: Afsprakenset
description: >
    Aanbevelingen voor zowel *KIK-V* als *iWlz (registers)*:

    - Zorg dat de beschrijving op **Bestuurlijk** niveau op elkaar aansluit (in termen, detailniveau en op te nemen onderwerpen).

    - Vanuit **Generiek** oogpunt is een **Conceptueel Model** van belang als overkoepelende ingang tot de beschrijving van de gegevens op detailniveau.

    - Termen en principes van *beschrijvende modellen* moeten beter op elkaar aansluiten

    - De beschrijving van de **Architectuur & Pve** op hetzelfde abstractievieau brengen en waar mogelijk en relevant naar dezelfde beschrijven infrastructuur componenten verwijzen (Techniek & Data)
      
      
    Specifieke aanbeveling voor *KIK-V* is het (her)modeleren van de huidige beschrijvingen zodat **Procesmodel**, **Procedures**, **Regels** en **Scenario's** kunnen worden onderscheiden. `NB: Dit zorgt ervoor dat deze op lange termijn beter te onderhouden zijn.`

    
bookCollapseSection: true
bookToC: true
weight: 2
---

{{< sectiontable >}}