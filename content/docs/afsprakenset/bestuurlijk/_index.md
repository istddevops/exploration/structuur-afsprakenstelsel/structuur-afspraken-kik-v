---
title: Bestuurlijk
description: >
    Aanbevelingen:
    
    - Om termen **Convenant** en **SLA's** in het iWlz-voorstel aan te passen aan de huidig termen die KIK-V hanteerd.
    
    - KIK-V zou **Relaesebeleid** als onderdeel van de Afsprakenset moeten opnemen.

bookCollapseSection: true
bookToC: false
weight: 1
---

{{< sectiontable >}}
