---
title: Releasebeleid
description: >
    Nog nader in te vullen binnen KIK-V. Prima om dit als extra paragraaf op te nemen in de Afsprakenset.
weight: 3
bookToC: false
---

In de huidige KIK-V Afsprakenset is het *Releasebeleid* beschreven in subparagraaf [Beheerafspraken / 4. Publiceren](https://kik-v.gitlab.io/afsprakenset/docs/beheerafspraken/#4-publiceren).

Daarin staan voornamelijk frequentie en gelijktijdigheid. Het *Releasebeleid* voor de Afsprakenset (en Uitwisselprofielen) is momenteel nog in ontwikkeling.