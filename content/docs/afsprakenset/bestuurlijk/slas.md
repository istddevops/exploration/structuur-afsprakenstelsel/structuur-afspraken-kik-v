---
title: SLA's
description: >  
    In *iWlz (registers)* kan daar ook voor de nadere invulling de term `Beheerafspraken`worden gebruikt. Daarmee voorstel om de term `SLA's` te veranderen naar `Beheerafspraken` (en daarbinnen naar de SLA's te verwijzen)
weight: 2
bookToC: false
---

In de huidige KIK-V Afsprakenset zijn `geen` *SLA's* opgenomen. Wel staan in volgende paragraaf [Beheerafspraken](https://kik-v.gitlab.io/afsprakenset/docs/beheerafspraken) een aantal SLA-achtige afspraken zoals wijzigingsproces en ontwikkelproces.