---
title: Convenant
description: >
    Er is over de nadere invulling van de `convenant` voldoende toelichting.
      
    In *iWlz (registers)* kan daar ook voor de nadere invulling de term `Sturingsstructuur` worden gebruikt. Daarmee voorstel om de term `Convenant` te veranderen naar `Sturingsstructuur` (en daarbinnen naar de Convenant te verwijzen)
weight: 1
bookToC: false
---

In de huidige KIK-V Afsprakenset is de *Convenant* beschreven in volgende paragrafen:

- [Sturingsstructuur](https://kik-v.gitlab.io/afsprakenset/docs/sturingsstructuur)