---
title: Architectuur & Pve
description: >
    Voor iWlz (regsiters) lijkt het erop dat het binnen de structuur de bedoeling is om binnen de Afsprakenset direct de *concretisering* van de **Architectuur & Pve** op te nemen. In de huidige KIK-V Afsprakenset worden daar vooral uitgangspunten benoemd. De aanbevelingen voor de toekomstige publicaties zijn:
    
    - De doorverwijzing naar de concretisering (Data & Techniek) binnen de de KIK-V Afsprakenset op te nemen

    - Binnen de nog te realiseren publicatie van iWlz (registers) en scheiding tussen uitgangspunten en concretisering aan te brengen.

weight: 4
bookToC: false
---

De huidige KIK-V Afsprakenset beschrijft:

- De uitgangspunten van de **Architectuur** zijn beschreven in paragraaf [Grondslagen / Grondplaat](https://kik-v.gitlab.io/afsprakenset/docs/grondslagen/grondplaat)

- De uitgangspunten voor het **Pve** zijn beschreven in [Grondslagen / Pricipes](https://kik-v.gitlab.io/afsprakenset/docs/grondslagen/principes)