---
title: Conceptueel Model
description: >
    In de huidige publicatie van de KIK-V Afsprakenset is *geen* vorm van een overal **Conceptueel Model** opgenomen. Aanbevelingen zijn:

    - Om te onderzoeken of een overkoepelend **Conceptueel Model** voor de **Modelgegevensset** (ook wel het Kennismodel) te realseren is. Deze zou vooral als uitleg en doorverwijzing moeten dienen.
    
    - In de nog te realiseren publicatie voor iWlz (registers) in ieder geval een vorm van een **Conceptueel Model** op te nemen.

weight: 3
bookToC: false
---

In huidige KIK-V Afsprakenset wordt alleen doorverwezen naar de Ontologie (ook wel kennismodel voor het Verpleeghuisdomein) in paragraaf [Modelgegevensset](https://kik-v.gitlab.io/afsprakenset/docs/modelgegevensset)