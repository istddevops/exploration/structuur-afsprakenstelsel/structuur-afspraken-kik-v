---
title: Uitwisselprofiel
description: >
    Voor iWlz (regsiters) is gekozen voor de nogal *specifieke* term **Netwerkfunctiemodel**. Binnen KIK-V is voor de wat meer generieke term **Modeluitwisselprofiel** gekozen. Aanbeveling om binnen iWlz (registers) de term **Modeluitwisselprofiel** over te nemen (of een nieuwe generieke term te kiezen).

weight: 3
bookToC: false
---

De huidige KIK-V Afsprakenset beschrijft:

- De uitgangspunten van de **Architectuur** zijn beschreven in paragraaf [Grondslagen / Grondplaat](https://kik-v.gitlab.io/afsprakenset/docs/grondslagen/grondplaat)

- De uitgangspunten voor het **Pve** zijn beschreven in [Grondslagen / Pricipes](https://kik-v.gitlab.io/afsprakenset/docs/grondslagen/principes)