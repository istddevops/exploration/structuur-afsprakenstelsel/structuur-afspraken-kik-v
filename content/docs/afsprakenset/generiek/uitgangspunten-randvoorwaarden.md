---
title: Uitgangspunten & Randvoorwaarden
description: >
     Verander de term **Doelen** naar **Uitgangspunten** om de volgende KIK-V Afspraakset publicaties aan te sluiten op de de huidige iStandaarden en iWlz (registers).

weight: 4
bookToC: false
---

In de huidige KIK-V Afsprakenset staan:

- De **Uitgangspunten** in paragraaf [Grondslagen / Doelen](https://kik-v.gitlab.io/afsprakenset/docs/grondslagen/citeria/doelen). `NB: De vorm waarin deze doelen zijn opgesteld lijkt op het stellen van *eisen op hoog nvieau* en daarmee hebben ze het karakter van *uitgangspunten*

- De **Randvoorwaarden** in paragraaf [Grondslagen / Randvoorwaarden](https://kik-v.gitlab.io/afsprakenset/docs/grondslagen/citeria/randvoorwaarden)