---
title: Generiek
description: >
    Aanbevelingen zijn tot nu toe:
    
    - Voor zowel *iWlz (registers)* als *KIK-V* zou een **Conceptueel Model** als overkoepelende ingang kunnen helpen om de samenhang tussen de uit te wisselen gegegevens inzichtelijk te maken.

    - De term **Modeluitwisselprofiel** kan dienen als overkoepelende term in plaats van **Netwerkfunctiemodel**. Of er moet een andere overkoepelende term worden bedacht (bijv. Model gegevensuitwisseling)
      
    - In een Afsprakenset zouden vanuit een hoog niveau beschrijving en/of pricipes naar de *concretisering* van de **Architectuur & Pve** moeten worden doorverwezen. Het is namelijk de bedoeling dat zowel *KIK-V* als *iWlz (registers)* voor veel als mogelijk van dezelfde architectuur-componenten gebruik kunnen maken (waaronder organisatorische, procesmatige, functionele en infrastructurele componenten)

bookCollapseSection: true
bookToC: false
weight: 2
---

{{< sectiontable >}}