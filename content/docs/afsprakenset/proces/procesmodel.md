---
title: Procesmodel
description: >
    In de volgende publicaties van de KIK-V Afsprakenset kunnen de *deelprocesmodellen* worden samengevoegd door deze in BPMN te rationaliseren als generiek **Procesmodel**. Van daaruit kan eventueel nog een specifieke uitwerking van 2 of meer deelprocessen worden gepubliceerd.

weight: 1
bookToC: false
---

De huidige KIK-V Afsprakenset is het **Procesmodel** momenteel vastgelegd als:

- Het **Operationele procesmodel** voor de gegevensuitwisseling in de [Uitwisselkalender](https://kik-v.gitlab.io/afsprakenset/docs/uitwisselkalender)

- Het **Beheerprocesmodel** voor de Afsprakenset en onderloggende onderdelen in [Proces Beheerafspraken](https://kik-v.gitlab.io/afsprakenset/docs/beheerafspraken/#proces)

Onder de [Beheerafspraken](https://kik-v.gitlab.io/afsprakenset/docs/beheerafspraken) zijn voor de [Ontwikkeling](https://kik-v.gitlab.io/afsprakenset/docs/beheerafspraken/ontwikkeling), een [Afwijkende uitvraag](https://kik-v.gitlab.io/afsprakenset/docs/beheerafspraken/afwijkende_uitvraag), [Onvoldoende anonimisatie](https://kik-v.gitlab.io/afsprakenset/docs/beheerafspraken/onvoldoende_anonimisatie) en [oetsing privacy- en informatiebeveiliging](https://kik-v.gitlab.io/afsprakenset/docs/beheerafspraken/privacy_en_informatiebeveiliging) beschrijvingen gemaakt met zowel procedures, regels als **Scenario's** (door elkaar heen).