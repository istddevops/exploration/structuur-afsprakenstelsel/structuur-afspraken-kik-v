---
title: Proces
description: >
    Aanbeveling om in volgende KIK-V publicaties eerst het **Procesmodel** in de BPMN-standaard te publiceren. Daarna kan iteratief aan de scheiding tussen **Procedures**, **Regels** en **Scenario's** toe worden gewerkt.

bookCollapseSection: true
bookToC: false
weight: 3
---

{{< sectiontable >}}