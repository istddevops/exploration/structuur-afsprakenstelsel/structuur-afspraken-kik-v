---
title: Scenarios
description: >
    In de volgende publicaties van de KIK-V Afsprakenset kunnen de **Scenario's** worden samengevoegd. Daarme ook **Procedures** en **Regels** in een meer overzichtelijke structuur in samenhang met het **Procesmodel** in lijn worden gebracht.

weight: 2
bookToC: true
---

Onder de [Beheerafspraken](https://kik-v.gitlab.io/afsprakenset/docs/beheerafspraken) zijn voor de [Ontwikkeling](https://kik-v.gitlab.io/afsprakenset/docs/beheerafspraken/ontwikkeling), een [Afwijkende uitvraag](https://kik-v.gitlab.io/afsprakenset/docs/beheerafspraken/afwijkende_uitvraag), [Onvoldoende anonimisatie](https://kik-v.gitlab.io/afsprakenset/docs/beheerafspraken/onvoldoende_anonimisatie) en [oetsing privacy- en informatiebeveiliging](https://kik-v.gitlab.io/afsprakenset/docs/beheerafspraken/privacy_en_informatiebeveiliging) beschrijvingen gemaakt met zowel **Procedures**, **Regels** als **Scenario's** (door elkaar heen).