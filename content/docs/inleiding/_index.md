---
title: Inleiding
description: >
    Aanbevelingen voor KIK-V:
    
    - Zorgen voor aansluiting van paragrafen per thema en/of onderwerp

    - Zorgen voor betere scheiding tussen doelstellingen en hoe deze worden vertaald naar te operationaliseren (ontwerp)principes
    
    - Zakelijk perspectief expliciet meenemen
      
      
    Algemene aanbeveling is de opbouw en termen in de **Toelichting** tussen *KIK-V* en *iWlz (registers)* zo veel als mogelijk op elkaar laten aansluiten en/of duidelijk aan te geven wat de verschillen zijn en waarom.

bookCollapseSection: true
weight: 1
---

{{< sectiontable >}}