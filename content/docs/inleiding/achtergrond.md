---
title: Achtergrond
description: >
    Achtergrond is over diverse niet aansluitende paragrafen verspreid. Het onderdeel onder *grondslagen* is deels een soort van *kwalitatieve Business Case*.
weight: 1
---

In de huidige KIK-V Afsprakenset is de *Achtergrond* beschreven in volgende paragrafen:

- [Introductie](https://kik-v.gitlab.io/afsprakenset)
- [Grondslagen / Achtergrond](https://kik-v.gitlab.io/afsprakenset/docs/grondslagen/achtergrond)