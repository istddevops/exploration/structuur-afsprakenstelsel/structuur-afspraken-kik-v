---
title: Business Case
description: >
    In de huidige publicatie worden vooral de *kwalitatieve* aspecten van de **Business Case** beschreven vanuit het *gebruikersperspectief*. 
      
    Maak de keuze om alleen de *kwalitatieve* aspecten te publiceren *expliciet* en verwijs naar de uitwerking vanuit *zakelijk perspectief* voor de *kwantitatieve* aspecten van de **Business Case** (project/programma documentatie).
weight: 4
---

In de huidige KIK-V Afsprakenset is de *Business Case* beschreven in volgende paragrafen:

- [Grondslagen / Achtergrond](https://kik-v.gitlab.io/afsprakenset/docs/grondslagen/achtergrond)
- [Gronslagen / Praktijkverhaal](https://kik-v.gitlab.io/afsprakenset/docs/grondslagen/praktijkverhaal)