---
title: Opdrachtformulering
description: >
    De *opdrachtformulering* is nu met de *principes* voor het ontwerp van de oplossing samengebracht onder de noemer van *criteria*. Aanbeveling om dit van elkaar te scheiden zodat helder wordt wat wordt *gevraagd* en hoe dit wordt *ingevuld*.
weight: 3
---

In de huidige KIK-V Afsprakenset is de *Opdrachtformulering* beschreven in volgende paragrafen:

- [Criteria / Doelen](https://kik-v.gitlab.io/afsprakenset/docs/grondslagen/citeria/doelen)
- [Critaria / Afbakening](https://kik-v.gitlab.io/afsprakenset/docs/grondslagen/afbakening)