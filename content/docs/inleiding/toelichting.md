---
title: Toelichting
description: >
    Er is over de samenstelling van de Afsprakenset voldoende toelichting.
      
    Aandachtspunt is dat de opbouw en termen die worden gebruikt in KIK-V en iWlz verschillen. Nader uit te werken wat `gemeenschappelijk` en wat de `verschillen` zijn (in functionaliteit en termen)
weight: 2
---

In de huidige KIK-V Afsprakenset is de *Toelichting* beschreven in volgende paragrafen:

- [Opbouw](https://kik-v.gitlab.io/afsprakenset/docs/opbouw)
- [Releaseinfo](https://kik-v.gitlab.io/afsprakenset/docs/releaseinfo)